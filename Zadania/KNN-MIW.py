import numpy as np
import random
import math
import operator

def dataSet(filename: str, trainingSet: list, testSet: list):
    trainingdata: list = np.loadtxt(filename, dtype=({'names':('x', 'y', 'z', 'd', 'm'), 'formats':('f', 'f', 'f', 'f', 'U25')}), delimiter=',')
        
    for i in range(len(trainingdata)):
        if random.random() < 0.7:
            trainingSet.append(trainingdata[i])
        else:
            testSet.append(trainingdata[i])

def euclideanDistance(instance1: list, instance2: list, length: int) -> float:
	distance: int = 0
	for x in range(length):
		distance += pow((instance1[x] - instance2[x]), 2)
	return math.sqrt(distance)

def getNeighbors(trainingSet: list, testInstance: list, k: int) -> list:
	distances: list = []

	for x in range(len(trainingSet)):
		dist = euclideanDistance(testInstance, trainingSet[x], len(testInstance)-1)
		distances.append((trainingSet[x], dist))
	
	distances.sort(key=operator.itemgetter(1))
	
	neighbors: list = []
	
	for x in range(k):
		neighbors.append(distances[x][0])
	
	return neighbors

def getResponse(neighbors: list) -> str:
	classVotes: dict = {}
	
	for x in range(len(neighbors)):
		response = neighbors[x][-1]
		if response in classVotes:
			classVotes[response] += 1
		else:
			classVotes[response] = 1
	
	sortedVotes: list = sorted(classVotes.items(), key=operator.itemgetter(1), reverse=True)
	
	return sortedVotes[0][0]

def getAccuracy(testSet: list, predictions: list) -> float:
	correct: int = 0
	
	for x in range(len(testSet)):
		if testSet[x][-1] == predictions[x]:
			correct += 1
	
	return (correct/float(len(testSet))) * 100.0
	

trainingSet: list= []
testSet: list= []

dataSet('C:/Users/citty/Downloads/iris.data', trainingSet, testSet)

print ('Train set length: ' + repr(len(trainingSet)))
print ('Test set length: ' + repr(len(testSet)))

predictions: list = []
k: int = 3

print('K is: ' + repr(k))

for x in range(len(testSet)):
	neighbors: list= getNeighbors(trainingSet, testSet[x], k)
	result: str= getResponse(neighbors)
	predictions.append(result)

accuracy: float= getAccuracy(testSet, predictions)

print('Accuracy: ' + repr(accuracy) + ' %')
	
