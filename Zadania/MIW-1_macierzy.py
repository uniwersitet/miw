"""
    Mnożenie macierzy
    Transpozycja macierzy
    Iloczyn skalarne
"""

def Multiply(X: list, Y: list) -> list:

    if len(X[0]) == len(Y):
        result: list = [[0 for j in range(len(X[0]))] for i in range(len(Y))]
        for i in range(len(X[0])):
            for j in range(len(Y)):
                for k in range(len(X[0])):
                    result[i][k] += X[i][k] * Y[k][j]
        return result
    else: 
        return "Not possible to multiply those matrixs"    


def Transposition(Tra: list) -> list:

    return [[Tra[j][i] for j in range(len(Tra))] for i in range(len(Tra[0]))]


def ScalarProduct(X: list, Y: list = None) -> int:

    if Y is None and len(X[0]) > 1:

        inter: int = 1
        for i in range(len(X[0])):
            nr: int = 1
            for j in range(len(X)):
                nr *= X[j][i]
            inter += nr
        return inter

    elif len(X) == len(Y):
        
        inter: int = 1
        for i in range(len(X)):
                inter += X[i] * Y[i]
        return inter

    else:
        return "impossible to complete"
    

#============================================================================

X: list =  [[1,3,5], [0,9,22], [2,8,7]]
Y: list =  [[4,6,9],[9,0,2],[9,8,6]]

M: list = [[2,3,4],[3,4,5]]
N: list = [[2,4],[5,6]]

print(Multiply(X, Y))
print(Multiply(Y,X))
print(Transposition(X))
print(Transposition(Y))

print(Multiply(M, N))
print(Multiply(N, M))
print(Transposition(M))
print(Transposition(N))

O: list = [1,2,3]
P: list = [4,5,6] 

print(ScalarProduct(O,P))
print(ScalarProduct(M))
print(ScalarProduct(X))