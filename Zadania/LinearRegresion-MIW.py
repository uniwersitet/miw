import numpy as np
import random as ran


def dataSet(filename: str, dictTest: dict, dictTraining: dict) -> dict:
    data: list = np.loadtxt(filename, dtype=({'names': (
        'x', 'y', 'z', 'd', 'm'), 'formats': ('f', 'f', 'f', 'f', 'U25')}), delimiter=',')

    Xtest: list = []
    Xtraining: list = []
    Ytest: list = []
    Ytraining: list = []

    for i in range(len(data)):
        app: list = []
        for k in range(len(data[i]) - 1):
            app.append(data[i][k])

        app.append(1)

        if ran.random() < 0.3:
            Xtest.append(app)
            Ytest.append(data[i][4])
        else:
            Xtraining.append(app)
            Ytraining.append(data[i][4])

    Ytest = mapY(Ytest)
    Ytraining = mapY(Ytraining)

    dictTest['X']: list = Xtest
    dictTest['Y']: list = Ytest
    dictTraining['X']: list = Xtraining
    dictTraining['Y']: list = Ytraining

# =================================================================


"""
    Mapuje wartości zmiennej opisywanej na liczbę
"""


def mapY(Y: list) -> list:

    X: list = np.array(Y)
    X: list = np.unique(X)

    for i in range(len(Y)):
        for k in range(len(X)):
            if Y[i] == X[k]:
                Y[i] = k + 1

    return(Y)

# ===================================================================


"""
    calculate the theta value theta = (Xt * X)^-1 * Xt * y
"""


def calculateTheta(X: list, Y: list):

    x: list = np.array(X)
    xt: list = np.transpose(x)

    the: list = np.dot(xt, x)
    the: list = np.linalg.inv(the)
    ta: list = np.dot(xt, np.array(Y))

    theta: list = np.dot(the, ta)

    return theta


"""
    Calculate the R2 value
"""


def calculateR2(mean_y: float, theta: list, X: list, Y: list) -> float:
    denom: float = 0
    numer: float = 0

    for i in range(len(X)):  # calculate sum(h(xi) - yi)^2
        h: float = 0
        for k in range(len(X[i])):
            x: float = (theta[k] * X[i][k])
            h += x

        denom += (h - Y[i])**2

    for i in range(len(Y)):  # calculate sum(mean y - yi)^2
        numer += (mean_y - Y[i])**2

    R2: float = 1 - (denom/numer)

    return R2

# =========================================================================


dictTraining: dict = {}
dictTest: dict = {}

dataSet('C:/Users/citty/Downloads/iris.data', dictTest, dictTraining)

theta = calculateTheta(dictTest['X'], dictTest['Y'])
mean_y = np.mean(dictTest['Y'])

R2 = calculateR2(mean_y, theta, dictTest['X'], dictTest['Y'])

lenYtest = len(dictTest['Y'])
lenYtraining = len(dictTraining['Y'])

print('Test data quantity: ' + repr(lenYtest))
print('Training data quantity: ' + repr(lenYtraining))
print('=========================================')
print('R2 is equal: ' + repr(R2))
print('Theta is equal: ' + str(theta))

thetaTrain = calculateTheta(dictTraining['X'], dictTraining['Y'])
mean_yTrain = np.mean(dictTraining['Y'])

R2Train = calculateR2(mean_yTrain, thetaTrain,
                      dictTraining['X'], dictTraining['Y'])

print('=========================================')
print('R2Train is equal: ' + repr(R2))
print('ThetaTrain is equal: ' + str(theta))
